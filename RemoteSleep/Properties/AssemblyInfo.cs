﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Remote Sleep")]
[assembly: AssemblyDescription("원격으로 컴퓨터 종료하기 - hosting.esukmean.com")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("E_Sukeman's")]
[assembly: AssemblyProduct("Remote Sleep")]
[assembly: AssemblyCopyright("Copyright © 이석민, 이석민닷컴 2015")]
[assembly: AssemblyTrademark("Remote Sleep")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(true)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("c3031c9d-4bae-4974-943c-3414ff4a7223")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.0.0.0")]
[assembly: AssemblyFileVersion("2.0.0.0")]
[assembly: NeutralResourcesLanguageAttribute("ko-KR")]
