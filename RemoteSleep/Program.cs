﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace RemoteSleep
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            System.Threading.Thread.Sleep(500); //시작시 0.5초 동안 대기 (부팅시 자동시작 상황용 - 딜레이를 둬서 부하 최소화)
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == "startup") System.Threading.Thread.Sleep(30000);
                //부팅시 자동시작된게 확실하면 30초 대기 (startup 인수 하나에 30초)
            }

A:
            try
            {
                if ((new System.Net.WebClient()).DownloadString("https://update.esukmean.com/apis/v1/version/remote-sleep") != "2-2")
                {
                    if (MessageBox.Show("Remote Sleep을 업데이트 해야합니다.\r\n\r\n업데이트 페이지로 이동하시겠습니까?\r\n(업데이트를 하기 전까지 프로그램이 종료됩니다.)", "Remote Sleep 업데이트 알림", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        System.Diagnostics.Process.Start((new System.Net.WebClient()).DownloadString("https://update.esukmean.com/open/download/remote-sleep"));
                    }

                    Environment.Exit(0);
                }
            }
            catch {
                System.Threading.Thread.Sleep(10000);
                goto A;
            }
            
            System.Diagnostics.Process[] proc;
            if ((proc = System.Diagnostics.Process.GetProcessesByName("RemoteSleep")).Length > 1)
            {
                int real_count = 0;
                for (int i = 0; i < proc.Length; i++)
                {
                    if (proc[i].Id != null) ++real_count; //이상하게 “부팅시 프로그램 자동시작”으로 켜지면 이상한게 하나더 잡혀서...
                }

                if (real_count > 1)
                {
                    if (MessageBox.Show("이미 Remote Sleep이 켜저 있습니다.\r\n켜저있는 프로그램들을 종료할까요?\r\n\r\n(아니오를 클릭하시면 이 프로그램이 닫힙니다.)", "프로그램 중복 실행", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        int current_pid = System.Diagnostics.Process.GetCurrentProcess().Id;

                        for (int i = 0; i < proc.Length; i++)
                        {
                            if (proc[i].Id != current_pid) proc[i].Kill();
                        }
                    }
                    else
                    {
                        Environment.Exit(0);
                    }
                }
            }

            RemoteSleep.Core core = new Core();
            if (Properties.Settings.Default.id != null && Properties.Settings.Default.id != "" && Properties.Settings.Default.pw != null && Properties.Settings.Default.pw != "")
            {
                core.login(Properties.Settings.Default.id, Properties.Settings.Default.pw, true);
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Form1 f1 = new Form1(ref core);
            if(core.is_login) f1.Visible = false;
            Application.Run(f1);
        }
    }
}
