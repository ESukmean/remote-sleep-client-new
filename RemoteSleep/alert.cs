﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RemoteSleep
{
    public partial class alert : Form
    {
        string alert_msg;
        bool closed_by_time = false;
        public alert(string alert)
        {
            InitializeComponent();
            alert_msg = alert;

            
            button1.Click += new EventHandler(button1_Click);
            
            FormClosing += new FormClosingEventHandler(noti_FormClosing);
            
            SetTopLevel(true);
            
            TopMost = false;
            
            TopLevel = false;
            
            TopMost = true;
            
            TopLevel = true;
            
            this.WindowState = FormWindowState.Normal;
            
            this.Load += alert_Load;
            
        }

        void alert_Load(object sender, EventArgs e)
        {
            CenterToScreen();   
            TopMost = false;
            TopMost = true; 
            BringToFront();

            CheckForIllegalCrossThreadCalls = false;
            new System.Threading.Thread(timeout).Start();
        }
        void timeout()
        {
            label1.Text = "30초 후에 컴퓨터를 " + alert_msg + " 할 예정입니다.\r\n취소하시려면 아래의 중지버튼을 눌려주세요.";
            label1.Location = new Point(this.Width / 2 - label1.Width / 2, label1.Location.Y);
            for (int time = 30; time > 0; time--)
            {
                Activate();
                BringToFront();
                label1.Text = time + "초 후에 컴퓨터를 " + alert_msg + " 할 예정입니다.\r\n취소하시려면 아래의 중지버튼을 눌려주세요.";
                System.Threading.Thread.Sleep(1000);
            }

            closed_by_time = true;
            this.Close();
        }

        void noti_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!closed_by_time) this.DialogResult = System.Windows.Forms.DialogResult.Abort;
        }
        void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
