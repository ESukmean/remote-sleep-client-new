﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RemoteSleep
{
	public enum command
	{
		no, //작업 없음
		shutdown, //종료
		restart, //재시작
		sleep, //절전모드
		HSleep, //최대 절전모드
		logout,
		Unknown
	}

	public class result
	{
		public bool res = false;
		public string msg = "";
	}
	public class result_check : result
	{
		public command type = command.Unknown;
	}

    public class Core
    {
        private int no; // -1이면 로그아웃 상태로 설정
        private string name;
        private string token;
        private bool logined;
        private System.Threading.Mutex mtx;
        private System.Net.WebClient WC;

        public Core()
        {
            WC = new System.Net.WebClient();
            mtx = new System.Threading.Mutex();
        }

        public bool is_login
        {
            get { return this.logined; }
        }
        public string get_name
        {
            get { return name; }
        }

        private void set_name(string name)
        {
            this.name = name;
        }
        public result login(string email, string pw, bool startup = false)
        {
			result res = new result();
			res.res = false;

            if (logined == true){
				res.msg = "이미 로그인 되어 있습니다.";
			}
            else if (email == ""){
				res.msg = "ID를 입력해 주십시오.";
			}
            else if (pw == "") {
				res.msg = "비밀번호를 입력해 주십시오.";
			}

			if(res.msg != "")
				return res;

            if(startup == false) pw = tools.sha512(email+'|'+pw);
            string result = "";

            try
            {
                int no = 0;
                try
                {
					Properties.Settings.Default.Reload();
                    if (Properties.Settings.Default.no != null)
                        no = Properties.Settings.Default.no;
                }
                catch
                {

                }
                result = tools.wc_msg("https://hosting.esukmean.com/api/v2/msg.esm?mode=login&email=" + email + "&key=" + pw + "&token=" + (token = tools.token_gen()) + (no != 0 ? "&no=" + no : ""), ref WC);
            }
            catch {
				res.msg = "서버와 교신중 오류가 발생하였습니다.";
				return res; 
			}

            if (result[0] == 'S')
            {
                string[] result_arr = result.Split('|');

				Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.no = Convert.ToInt32(result_arr[1]);
                this.no = Properties.Settings.Default.no;
				Properties.Settings.Default.Save();
				
                this.name = result_arr[2];
                this.logined = true;

                res.res = true;
				return res;
            }

			res.msg = result.Substring(1);
			return res;
        }
        public result logout()
        {
			result res = new result();
			res.res = false;
            if (this.logined == false){
				res.msg = "로그인 되어있지 않습니다.";
				return res;
			}
            logined = false;
            no = -1;

			Properties.Settings.Default.Reset();
			Properties.Settings.Default.Save();
			Properties.Settings.Default.Reload();

			res.res = true;
            return res;
        }
        public result change_name(string name)
        {
			result res = new result();
			res.res = false;
            if (logined == false){
				res.msg = "로그인 되어있지 않습니다.";
				return res;
			}

            string result = "";
            mtx.WaitOne();
            try
            {
                result = tools.wc_msg("https://hosting.esukmean.com/api/v2/msg.esm?mode=change_name&token=" + token + "&no=" + no + "&name=" + name, ref WC);
            }
            catch(Exception ex)
            {
                mtx.ReleaseMutex(); //왠지 이 꼴 나중에 코드 다시볼때 내가 왜 이리 썼지 후회할만한 코드...
                res.msg = "오류: " + ex.Message;
				return res;
            }
            mtx.ReleaseMutex();

            if (result[0] == 'S')
            {
                set_name(name);
                res.res = true;
            }else{
				res.msg = "오류: " + result.Substring(1);
			}

            return res;
        }
        public result_check check_msg()
        {
			result_check res = new result_check();
			res.res = false;

            if (no == -1 && logined == false){
				res.res = true;
				res.msg = "로그아웃 되었습니다.";
				res.type = command.logout;
				return res;
			}

            if (logined == false){
				res.msg = "로그인 되어있지 않습니다.";
				return res;
			}

            mtx.WaitOne();
            string result = "";
            try
            {
                result = tools.wc_msg("https://hosting.esukmean.com/api/v2/msg.esm?mode=check&no=" + no + "&token=" + token, ref WC);
            }
            catch(Exception ex)
            {
				res.msg = ex.Message;
                mtx.ReleaseMutex();
                return res;
            }
            mtx.ReleaseMutex();

            if (result[0] == 'E'){
				res.msg = result.Substring(1);
				return res;
			}

            string[] result_arr = result.Split('|');
			if (name != result_arr[1])
			{
				res.msg = result_arr[1];
				set_name(result_arr[1]);
			}

			res.res = true;
            switch (result_arr[2])
            {
                case "0":
					res.type = command.no;
					break;

                case "1":
					res.type = command.shutdown;
					break;

                case "2":
					res.type = command.sleep;
					break;

                case "3":
					res.type = command.restart;
					break;

                case "4":
					res.type = command.HSleep;
					break;

                default:
					res.res = false;
					res.msg = "알 수 없는 오류가 발생하였습니다.";
					break;
            }

			return res;
        }
    }

    public static class tools
    {
        public static string sha512(string str)
        {
            System.Security.Cryptography.SHA512Managed sha = new System.Security.Cryptography.SHA512Managed();
            return Convert.ToBase64String(sha.ComputeHash(System.Text.Encoding.UTF8.GetBytes(str)));
        }
        public static string token_gen()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, 32).Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public static string wc_msg(string url, ref System.Net.WebClient wc)
        {
            byte[] data = wc.DownloadData(url);
            return System.Text.Encoding.UTF8.GetString(data).Trim();
        }
    }
}