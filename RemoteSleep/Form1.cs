﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RemoteSleep
{
    public partial class Form1 : Form
    {
        private bool thread_flag = false;
        private Core core;
        public Form1(ref Core pcore)
        {
            this.core = pcore;
            InitializeComponent();
			checkBox2.Checked = Properties.Settings.Default.AutoLogin == null ? true : Properties.Settings.Default.AutoLogin;

            if (core.is_login)
            {
                login_panel.Visible = false;
                main_panel.Visible = true;

                checkBox2.Checked = true; //프로그램 시작시 자동 로그인 체크박스
                textBox2.Text = core.get_name;
                Opacity = 0;
                this.Hide();
                this.Load += new EventHandler(Form1_Load);
                show_noti(true);

                start_monitor();
            }
            else
            {
                login_panel.Visible = true;
                main_panel.Visible = false;
            }

            string path = "";
            object rk;
            if ((rk = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true).GetValue("Remote_Sleep")) != null && (path = rk.ToString()) != "") checkBox1.Checked = true;
            //자동 시작 목록에 remote Sleep이 존재하면 “윈도우 시작시 자동 실행” 에 체크.
            if (path != "" && path != Application.ExecutablePath.Replace("\"startup\"", "")) { Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true).SetValue("Remote_Sleep", Application.ExecutablePath + " \"startup\""); }
            //자동시작에 있으나 그 경로가 이 프로그램과 동일치 않으면 부팅시 자동시작되는 프로그램을 이 프로그램으로 설정

            checkBox1.CheckedChanged += new EventHandler(checkBox1_CheckedChanged);
            checkBox2.CheckedChanged += new EventHandler(checkBox2_CheckedChanged);
            this.FormClosing += new FormClosingEventHandler(Form1_FormClosing);
            label1.Click += new EventHandler(label1_Click);
            label3.Click += new EventHandler(label3_Click);
            button1.Click += new EventHandler(button1_Click);
            button2.Click += new EventHandler(button2_Click);
            button3.Click += new EventHandler(button3_Click);

			CheckForIllegalCrossThreadCalls = false;
        }

        void label3_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.esukmean.com");
        }

        void start_monitor()
        {
            thread_flag = true;
            new System.Threading.Thread(monitor).Start();
        }
        void monitor()
        {
            System.Threading.Thread.Sleep(3000);

			result_check res;
			int error_count = 0;
            while (thread_flag && core.is_login)
            {
				System.Threading.Thread.Sleep(4500);
				res = core.check_msg();

				if (res.res == false)
				{
					if (++error_count == 5)
					{
						MessageBox.Show("Remote Sleep - 오류가 발생했습니다:\r\n" + res.msg + "\r\n\r\n혹시모를 상황에 대비하여 로그아웃 되었습니다.", "Remote Sleep - 오류안내", MessageBoxButtons.OK, MessageBoxIcon.Error);

						core.logout();
						login_panel.Visible = true;
						main_panel.Visible = false;
						break;

					}
					continue;
				}

				if (res.msg != "")
					textBox2.Text = res.msg;

                switch (res.type)
                {
                    case command.no:
                        break;

                    case command.shutdown:
                        {
                            alert alert = new alert("종료");
                            alert.TopMost = true;
                            alert.ShowDialog();

                            if (alert.DialogResult != System.Windows.Forms.DialogResult.Abort)
                                System.Diagnostics.Process.Start(System.Environment.SystemDirectory + "\\shutdown.exe", "/s /t 10 /c \"Remote Sleep (hosting.esukmean.com) - 원격지에서 명령을 내렸습니다.\"");
                            break;
                        }
                    case command.sleep:
                        {
                            alert alert = new alert("절전모드로 전환");
                            alert.TopMost = true;
                            alert.ShowDialog();

                            if (alert.DialogResult != System.Windows.Forms.DialogResult.Abort)
                                Application.SetSuspendState(PowerState.Suspend, true, true);
                             break;
                        }
                    case command.restart:
                        {
                            alert alert = new alert("재시작");
                            alert.TopMost = true;
                            alert.ShowDialog();

                            if (alert.DialogResult != System.Windows.Forms.DialogResult.Abort)
                                System.Diagnostics.Process.Start(System.Environment.SystemDirectory + "\\shutdown.exe", " /r /t 10 /c \"Remote Sleep (hosting.esukmean.com) - 원격지에서 명령을 내렸습니다.\"");
                            break;
                        }
                    case command.HSleep:
                        {
                            alert alert = new alert("최대절전 모드로 전환");
                            alert.TopMost = true;
                            alert.ShowDialog();

                            if (alert.DialogResult != System.Windows.Forms.DialogResult.Abort)
                                Application.SetSuspendState(PowerState.Hibernate, true, true);
                            break;
                        }
                    case command.logout:
                        break;

                    default:
                        break;
                }
            }

            thread_flag = false;
        }

        void hide()
        {
            Visible = false;
            ShowInTaskbar = false;
            this.Hide();
        }
        void show()
        {
            Visible = true;
            ShowInTaskbar = true;
            this.Show();

            notifyIcon1.Visible = false;
            this.BringToFront();
        }

        public void show_noti(bool auto)
        {
            notifyIcon1.Visible = true;
            notifyIcon1.Click += new EventHandler(notifyIcon1_Click);
            if (auto) return;

            notifyIcon1.BalloonTipTitle = "Remote Sleep이 닫혔습니다.";
            notifyIcon1.BalloonTipText = "Remote Sleep을 로그인한 상태로 창을 닫으면 프로그램이 종료되는 대신에, 여기에 남겨집니다.\n완전히 프로그램을 종료하고 싶으면 로그아웃후 창을 닫아주시길 바라며, 이 아이콘을 누르시면 창이 다시 나타납니다.";
            notifyIcon1.BalloonTipIcon = ToolTipIcon.Info;

            notifyIcon1.ShowBalloonTip(3000);
        }

        void notifyIcon1_Click(object sender, EventArgs e)
        {
            show();
        }
        void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (core.is_login == false) return;

            e.Cancel = true;
            hide();
            show_noti(false);
        }

        void button1_Click(object sender, EventArgs e)
        {
			result res;

			string pw = maskedTextBox1.Text;
			maskedTextBox1.Text = "";

            if ((res = core.login(textBox1.Text, pw)).res == true)
            {
                login_panel.Visible = false;
                main_panel.Visible = true;
                textBox2.Text = core.get_name;

                if (checkBox2.Checked)
                {
                    Properties.Settings.Default.id = textBox1.Text;
                    Properties.Settings.Default.pw = tools.sha512(textBox1.Text + "|" + pw);
					Properties.Settings.Default.AutoLogin = true;

                    Properties.Settings.Default.Save();
                }

                textBox1.Text = "";
                start_monitor();
				
				
				return;
            }

            MessageBox.Show(res.msg, "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        void button2_Click(object sender, EventArgs e)
        {
            if (core.is_login == false) { MessageBox.Show("로그인 되어 있지 않습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error); return; }

			result res = core.logout();
            if (res.res == true)
            {
				login_panel.Visible = true;
                main_panel.Visible = false;

				return;
			}
               
			MessageBox.Show(res.msg, "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);

            
        }
        void button3_Click(object sender, EventArgs e)
        {
			result res;

			if ((res = core.change_name(textBox2.Text)).res == true){
				MessageBox.Show("정상적으로 변경되었습니다.", "성공", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}

			MessageBox.Show(res.msg, "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        void label1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://hosting.esukmean.com/");
        }
            
        void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            Microsoft.Win32.RegistryKey rkApp = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            if (checkBox1.Checked)
            {
                rkApp.SetValue("Remote_Sleep", Application.ExecutablePath + " \"startup\"");
                MessageBox.Show("앞으로 Remote Sleep이 부팅시 마다 켜집니다.", "알림", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                rkApp.DeleteValue("Remote_Sleep", false);
                MessageBox.Show("앞으로 Remote Sleep이 부팅시 마다 켜지지 않습니다.", "알림", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (login_panel.Visible) return; 
            //짜피 로그인 할때 체크박스에 체크되어 있는가 확인하기에
            //번거롭게 2번 Email, PW 치기 보다는 로그인 할때 그냥 같이 저장하도록.

            if (checkBox2.Checked == false)
            {
                Properties.Settings.Default.id = "";
                Properties.Settings.Default.pw = "";
                
                Properties.Settings.Default.Save();
            }
            else
            {
                DialogResult dr = MessageBox.Show("다시한번 로그인 해야 자동 로그인 기능을 사용하실 수 있습니다.\r\n다시 로그인 하시겠습니까?", "귀차니즘이 있으십니까?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                
                if(dr == System.Windows.Forms.DialogResult.OK) button2_Click(null, null);
            }
        }

        void Form1_Load(object sender, EventArgs e)
        {
            if (core.is_login == false) return;
            
            hide();
            this.Opacity = 1;
        }
    }
}
